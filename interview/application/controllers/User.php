<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	// - Cách lấy dữ liệu db		  
	// $sql = "";
	// $rs = $this->db->query($sql);
	// $rs = $rs->result(); // array list
	// $rs = $rs->row(); // object one item

	// if query is update, insert
	// => $flag = $this->db->query($sql);


	// - Cách lấy dữ liệu post, get
	// $id = $this->input->get('id');
	// $id = $this->input->post('id');

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$sql = "";
		$results = $this->db->query($sql)->result();
		$this->load->view('list_user', [
			'list' => $results
		]);
	}

	public function edit($id)
	{
		// http://localhost/interview/user/edit/1
	}

	public function ajax_get_list()
	{
		// http://localhost/interview/user/ajax_get_list
		$group_id = $this->input->get('group_id');
		$store_id = $this->input->get('store_id');
		// để lấy giá trị
		$results = [];
		// lấy ds
		// $sql = "";
		// $results = $this->db->query($sql)->result();

		die(json_encode(['status' => 1, 'data' => $results, 'message' => 'success']));
	}

	public function ajax_get_user_detail()
	{
		// http://localhost/interview/user/ajax_get_user_detail
		$id 	= $this->input->get('id');
		$sql 	= "";
		$item 	= $this->db->query($sql)->row();
		die(json_encode(['status' => 1, 'data' => $item, 'message' => 'success']));
	}
}
